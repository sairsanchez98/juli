<!DOCTYPE html>
<!-- 
Template Name: Brunette - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: https://hencework.ticksy.com/

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>JULI</title>
    <meta name="description" content="JULI | INICIAR" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    
    <script>var SERVERURL = "<?php echo SERVERURL; ?>";</script>
    <!-- Toastr CSS -->
    <link href="<?php echo SERVERURL; ?>Assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="<?php echo SERVERURL; ?>Assets/dist/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!-- Preloader -->
    <div class="preloader-it"></div>
    <!-- /Preloader -->

	<!-- HK Wrapper -->
	<div class="hk-wrapper" id="login">

          <!-- Main Content -->
        <div class="hk-pg-wrapper">
			<!-- Container -->
            <div class="container mt-xl-50 mt-sm-30 mt-15">
                <!-- Title -->
                <div class="hk-pg-header">
                    <div>
						<h2 class="">Wellcome to Juli!</h2>
					</div>
					<div class="d-flex mb-0 flex-wrap">
                        <div class="btn-group btn-group-sm btn-group-rounded mb-15 mr-15" role="group">
							<button :class="[class_btn_registrarme, 'btn btn-sm  btn-rounded btn-wth-icon icon-wthot-bg mb-15']"
                                    @click="class_btn_registrarme = 'btn-primary' , class_btn_tengo_cuenta='btn-outline-primary' , SelectViewRegister('selectRol')">
                                <span class="icon-label">
                                    <span class="feather-icon">
                                        <i data-feather="feather"></i>
                                    </span> 
                                </span>
                                <span class="btn-text">Registrarme</span>
                            </button>
							<button :class="[class_btn_tengo_cuenta, 'btn btn-sm  btn-rounded btn-wth-icon icon-wthot-bg mb-15']"
                                    @click="class_btn_registrarme = 'btn-outline-primary' , class_btn_tengo_cuenta='btn-primary' , SelectViewRegister('iniciar_sesion')">
                                <span class="icon-label">
                                    <span class="feather-icon">
                                        <i data-feather="check"></i>
                                    </span> 
                                </span>
                                <span class="btn-text">Tengo una cuenta</span>
                            </button>
						</div>
						
                    </div>
                </div>
                <!-- /Title -->


                <!-- FORM REGISTRO SELECCIONAR ROL-->
                <div class="row" v-if="registro[0]['select_rol']">
                    <div class="col-xl-12">
                        <div class="hk-row">
							<div class="col-lg-4 col-sm-6 " @click="SelectViewRegister('colegio')">
								<div class="card card-sm">
                                    <div :class="[class_card_colegio,'card-body']" @mouseover="hoverCards('colegio')" @mouseout="hoverOutCards('colegio')">
										<div class="d-flex align-items-center justify-content-between position-relative row">
											<div class="col-md-4 col-sm-4 col-xs-2">
												<img width="100%" src="<?php echo SERVERURL. "Assets/dist/img/iconos/login_colegio.svg";  ?>">
											</div>
											<div class="col-md-8 col-sm-8 col-xs-8">
												<span class="d-block display-5 font-weight-400 ">Soy un colegio</span>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-4 col-sm-6" @click="SelectViewRegister('docente')">
								<div class="card card-sm">
									<div :class="[class_card_docente,'card-body']" @mouseover="hoverCards('docente')" @mouseout="hoverOutCards('docente')">
										<div class="d-flex align-items-center justify-content-between position-relative row">
											<div class="col-md-4">
												<img width="100%" src="<?php echo SERVERURL. "Assets/dist/img/iconos/login_docente.svg";  ?>">
											</div>
											<div class="col-md-8">
												<span class="d-block display-5 font-weight-400 ">Soy docente</span>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-4 col-sm-6" @click="SelectViewRegister('estudiante')">
								<div class="card card-sm">
									<div :class="[class_card_estudiante,'card-body']" @mouseover="hoverCards('estudiante')" @mouseout="hoverOutCards('estudiante')">
										<div class="d-flex align-items-center justify-content-between position-relative row">
											<div class="col-md-4">
												<img width="100%" src="<?php echo SERVERURL. "Assets/dist/img/iconos/login_estudiante.svg";  ?>">
											</div>
											<div class="col-md-8">
												<span class="d-block display-5 font-weight-400 ">Soy estudiante</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
						</div>
					
					
					</div>
                </div>

                <!-- FORM REGISTRO ROL ESTUDIANTE , COLEGIO O DOCENTE -->
                <div class="row" v-if="registro[0]['form_estudiante'] || registro[0]['form_docente'] || registro[0]['form_colegio']">
                    <div class="col-xl-6">
                        <div class="hk-row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <input v-if="registro[0]['form_estudiante']" v-model="nombres_estudiante" type="text" class="form-control rounded-input mt-15" placeholder="Nombre(s) estudiante">
                                            <input v v-if="registro[0]['form_estudiante']" v-model="apellidos_estudiante" type="text" class="form-control rounded-input mt-15" placeholder="Apellidos estudiante">
                                            <input v-if="registro[0]['form_docente']" v-model="nombres_docente" type="text" class="form-control rounded-input mt-15" placeholder="Nombre(s) Docente">
                                            <input v-if="registro[0]['form_docente']" v-model="apellidos_docente" type="text" class="form-control rounded-input mt-15" placeholder="Apellidos Docente">
                                            <input v-if="registro[0]['form_colegio']" v-model="nombre_colegio" type="text" class="form-control rounded-input mt-15" placeholder="Nombre colegio">
                                        </div>
                                        <div class="col-md-12">
                                            <input v-model="correo" type="text" class="form-control rounded-input mt-15" placeholder="Correo electrónico">
                                        </div>
                                        <div class="col-md-12">
                                            <input v-model="contrasena" type="password" class="form-control rounded-input mt-15" placeholder="Contraseña">
                                        </div>
                                        
                                        <div class="col-md-12 mt-20">
                                            <button v-if="!procesando" @click="RegistrarUsuario()" type="button" class="btn btn-rounded btn-outline-primary btn-block">Continuar</button>
                                            <button v-if="procesando" type="button" class="btn btn-rounded btn-primary btn-block">
                                                <img class="img-fluid" width="16" src="<?php echo SERVERURL; ?>Assets/dist/img/loader.gif"> Cargando
                                            </button>
                                        </div>
                                            
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <div class="hk-row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <h5 v-if="registro[0]['form_estudiante']" class="card-header" v-text="'Registro estudiantes'"></h5>
                                    <h5 v-if="registro[0]['form_docente']" class="card-header" v-text="'Registro docentes'"></h5>
                                    <h5 v-if="registro[0]['form_colegio']" class="card-header" v-text="'Registro colegios'"></h5>
                                    <div class="card-body">
                                        <blockquote class="blockquote mb-0">
                                            <div v-if="registro[0]['form_estudiante']" class="w-100 bg-light mt-10 mb-10">
                                                <img width="60" src="<?php echo SERVERURL; ?>Assets/dist/img/iconos/login_estudiante.svg" class="img-fluid mx-auto d-block" alt="img">
                                                <p class="text-center">¡Al registrarte como estudiante te divertirás  con sorprendentes juegos que te permitirán aprender el idioma Inglés super fácil!</p>
                                            </div>
                                            <div v-if="registro[0]['form_docente']" class="w-100 bg-light mt-10 mb-10">
                                                <img width="60" src="<?php echo SERVERURL; ?>Assets/dist/img/iconos/login_docente.svg" class="img-fluid mx-auto d-block" alt="img">
                                                <p class="text-center">¡Al registrarte como docente podrás vincular a tus estudiantes  para gestionar juntos el aprendizaje del idioma Inglés super fácil!</p>
                                            </div>
                                            <div v-if="registro[0]['form_colegio']" class="w-100 bg-light mt-10 mb-10">
                                                <img width="60" src="<?php echo SERVERURL; ?>Assets/dist/img/iconos/login_colegio.svg" class="img-fluid mx-auto d-block" alt="img">
                                                <p class="text-center">¡Al registrarte como colegio podrás vincular a docentes y estudiantes para gestionar juntos el aprendizaje del idioma Inglés super fácil!</p>
                                            </div>
                                            <button type="button" class="btn btn-rounded btn-info btn-block">
                                                    <i class="fa fa-play"></i>
                                                 ¿Ver un tutorial?
                                            </button>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                
                
                
                <!-- FORM INICIAR SESIÓN-->
                <div class="row" v-if="iniciar_sesion">
                    <div class="col-xl-6">
                        <div class="hk-row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <input v-model="correo_" type="text" class="form-control rounded-input mt-15" placeholder="Usuario / correo">
                                        </div>
                                        <div class="col-md-12">
                                            <input v-model="contrasena_" type="password" class="form-control rounded-input mt-15" placeholder="Contraseña">
                                        </div>
                                        
                                        <div class="col-md-12 mt-20">
                                            <button v-if="!procesando" @click="iniciarSesion()" type="button" class="btn btn-rounded btn-outline-primary btn-block">Iniciar sesión</button>
                                            <button v-if="procesando" type="button" class="btn btn-rounded btn-primary btn-block">
                                                <img class="img-fluid" width="16" src="<?php echo SERVERURL; ?>Assets/dist/img/loader.gif"> Cargando
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <div class="hk-row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <h5 class="card-header">¿Problemas?</h5>
                                    <div class="card-body">
                                        <blockquote class="blockquote mb-0">
                                            <button type="button" class="btn btn-rounded btn-info btn-block">Olvidé mi contraseña</button>
                                            <button type="button" class="btn btn-rounded btn-info btn-block">¿Cómo iniciar sesión?</button>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



            </div>
            <!-- /Container -->
			
            <!-- Footer -->
            <div class="hk-footer-wrap container">
                <footer class="footer">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <p>Developed by<a href="https://array.com.col" class="text-dark" target="_blank">ARRAY TIC & EDUCOLOMBIA</a> © <?php echo date("Y")?></p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <p class="d-inline-block">Follow us</p>
                            <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                            <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                            <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- /Footer -->
        </div>
        <!-- /Main Content -->

    </div>
    <!-- /HK Wrapper -->

    <!-- jQuery -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/jquery/dist/jquery.min.js"></script>

	<!-- VUE Y AXIOS -->
	<script src="<?php echo SERVERURL; ?>Assets/vue.js"></script>
	<script src="<?php echo SERVERURL; ?>Assets/axios.min.js"></script>
	
    <!-- FeatherIcons JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/dist/js/feather.min.js"></script>

    <!-- Toastr JS -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    

	<script src="<?php echo SERVERURL; ?>Views/JS/Login.js"></script>


    <!-- Init JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/dist/js/init.js"></script>
	
	
</body>

</html>