<?php  $USER_LOG = $_SESSION["UserLoggedIn"]["user"]; ?>
<!DOCTYPE html>
<!-- 
Template Name: Brunette - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: https://hencework.ticksy.com/

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>JULI</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
	
    <!-- Bootstrap Dropzone CSS -->
    <link href="<?php echo SERVERURL; ?>Assets/vendors/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>
    

	<!-- Morris Charts CSS -->
    <link href="<?php echo SERVERURL; ?>Assets/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
	
    <!-- Toggles CSS -->
    <link href="<?php echo SERVERURL; ?>Assets/vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo SERVERURL; ?>Assets/vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">
	
	<!-- Toastr CSS -->
    <link href="<?php echo SERVERURL; ?>Assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
	<link href="<?php echo SERVERURL; ?>Assets/dist/css/style.css" rel="stylesheet" type="text/css">
    
	
	 <!-- jQuery -->
	 <script src="<?php echo SERVERURL; ?>Assets/vendors/jquery/dist/jquery.min.js"></script>
     <script>var SERVERURL = "<?php echo SERVERURL; ?>";</script>
	<!-- VUE Y AXIOS -->
	<script src="<?php echo SERVERURL; ?>Assets/vue.js"></script>
	<script src="<?php echo SERVERURL; ?>Assets/axios.min.js"></script>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->
	
	<!-- HK Wrapper -->
	<div class="hk-wrapper hk-horizontal-nav">
		 <!-- Setting Panel -->
		<div class="hk-settings-panel">
            <div class="nicescroll-bar position-relative">
                <div class="settings-panel-wrap">
                    <div class="settings-panel-head">
                        <img class="brand-img d-inline-block align-top" src="dist/img/logo-light.png" alt="brand" />
                        <a href="javascript:void(0);" id="settings_panel_close" class="settings-panel-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
                    </div>
                    <hr>
                    <h6 class="mb-5">Layout</h6>
                    <p class="font-14">Choose your preferred layout</p>
                    <div class="layout-img-wrap">
                        <div class="row">
                            <a href="dashboard1.html" class="col-6 mb-30">
                                <img class="rounded opacity-70" src="dist/img/layout1.png" alt="layout">
                                <i class="zmdi zmdi-check"></i>
                            </a>
                            <a href="javascript:void(0);" class="col-6 mb-30 active">
                                <img class="rounded opacity-70" src="dist/img/layout2.png" alt="layout">
                                <i class="zmdi zmdi-check"></i>
                            </a>
                           <a href="dashboard3.html" class="col-6 mb-30">
                                <img class="rounded opacity-70" src="dist/img/layout3.png" alt="layout">
                                <i class="zmdi zmdi-check"></i>
                            </a>
							<a href="dashboard4.html" class="col-6 mb-30">
                                <img class="rounded opacity-70" src="dist/img/layout4.png" alt="layout">
                                <i class="zmdi zmdi-check"></i>
                            </a>
							<a href="dashboard5.html" class="col-6">
                                <img class="rounded opacity-70" src="dist/img/layout5.png" alt="layout">
                                <i class="zmdi zmdi-check"></i>
                            </a>
                        </div>
                    </div>
                    <hr>
                    <h6 class="mb-5">Navigation</h6>
                    <p class="font-14">Menu comes in two modes: dark & light</p>
                    <div class="button-list hk-nav-select mb-10">
                        <button type="button" id="nav_light_select" class="btn btn-outline-primary btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i class="fa fa-sun-o"></i> </span><span class="btn-text">Light Mode</span></button>
                        <button type="button" id="nav_dark_select" class="btn btn-outline-light btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i class="fa fa-moon-o"></i> </span><span class="btn-text">Dark Mode</span></button>
                    </div>
                    <hr>
                    <h6 class="mb-5">Top Nav</h6>
                    <p class="font-14">Choose your liked color mode</p>
                    <div class="button-list hk-navbar-select mb-10">
                        <button type="button" id="navtop_light_select" class="btn btn-outline-light btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i class="fa fa-sun-o"></i> </span><span class="btn-text">Light Mode</span></button>
                        <button type="button" id="navtop_dark_select" class="btn btn-outline-primary btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i class="fa fa-moon-o"></i> </span><span class="btn-text">Dark Mode</span></button>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-between align-items-center">
                        <h6>Scrollable Header</h6>
                        <div class="toggle toggle-sm toggle-simple toggle-light toggle-bg-primary scroll-nav-switch"></div>
                    </div>
                    <button id="reset_settings" class="btn btn-primary btn-block btn-reset mt-30">Reset</button>
                </div>
            </div>
            <img class="d-none" src="dist/img/logo-light.png" alt="brand" />
            <img class="d-none" src="dist/img/logo-dark.png" alt="brand" />
        </div>
        <!-- /Setting Panel -->

	  <?php 
		include "Views/Components/EstudianteTopNav.php"; 
		$View = CtrlTemplate::CargarRuta();
		include $View;
	  ?>

       

        <!-- Main Content -->
		
        <!-- /Main Content -->

    </div>
    <!-- /HK Wrapper -->

   
    
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/dist/js/jquery.slimscroll.js"></script>

    <!-- Fancy Dropdown JS -->
    <script src="<?php echo SERVERURL; ?>Assets/dist/js/dropdown-bootstrap-extended.js"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/dist/js/feather.min.js"></script>

    <!-- Toggles JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/jquery-toggles/toggles.min.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/dist/js/toggle-data.js"></script>
	
	<!-- Toastr JS -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    
	<!-- Counter Animation JavaScript -->
	<script src="<?php echo SERVERURL; ?>Assets/vendors/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="<?php echo SERVERURL; ?>Assets/vendors/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Morris Charts JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/vendors/morris.js/morris.min.js"></script>
	
	<!-- Easy pie chart JS -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
	
	<!-- Flot Charts JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/flot/excanvas.min.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/vendors/flot/jquery.flot.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/vendors/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/vendors/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/vendors/flot/jquery.flot.time.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/vendors/flot/jquery.flot.stack.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/vendors/flot/jquery.flot.crosshair.js"></script>
    <script src="<?php echo SERVERURL; ?>Assets/vendors/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
	
	<!-- EChartJS JavaScript -->
	<script src="<?php echo SERVERURL; ?>Assets/vendors/echarts/dist/echarts-en.min.js"></script>
	

	<!-- Owl JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/owl.carousel/dist/owl.carousel.min.js"></script>

    <!-- Owl Init JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/dist/js/owl-data.js"></script>
    
    <!-- Init JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/dist/js/init.js"></script>
	
	
</body>

</html>