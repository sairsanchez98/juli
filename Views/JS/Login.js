
var login = new Vue({
    el: "#login",
    data: {
    // COMPONENTES
    registro:[
        {"select_rol":true , "form_colegio": false, "form_docente":false, "form_estudiante":false}
    ],
    iniciar_sesion: false,

    // CLASES DIV LOGIN: 
    class_card_colegio : ["bg-white" , "text-dark"],
    class_card_docente : ["bg-white" , "text-dark"],
    class_card_estudiante : ["bg-white" , "text-dark"],

    // CLASES BUTTONS (registro & ya tengo una cuenta)
    class_btn_registrarme : "btn-primary",
    class_btn_tengo_cuenta : "btn-outline-primary",

    // DATOS DE REGISTRO :
    nombres_estudiante: "",
    apellidos_estudiante: "",
    nombres_docente: "",
    apellidos_docente:"",
    nombre_colegio: "",
    correo : "",
    contrasena: "",
    rol: "",

    /// DATOS INICIAR SESION
    correo_ : "",
    contrasena_ : "",

    // DATOS DE PROCESO:
    procesando: false


    },
    methods: {
      
        // Hover clases CARDS
        hoverCards : function (card){
            if(card == "colegio"){login.class_card_colegio = ["bg-blue-light-3" ,"cursor_pointer", "text-white"]}
            if(card == "docente"){login.class_card_docente = ["bg-blue-light-3" ,"cursor_pointer", "text-white"]}
            if(card == "estudiante"){login.class_card_estudiante = ["bg-blue-light-3" ,"cursor_pointer", "text-white"]}
        },

        // HoverOut clases CARDS
        hoverOutCards: function (card){
            if(card == "colegio"){login.class_card_colegio = ["bg-white" , "text-dark"]}
            if(card == "docente"){login.class_card_docente = ["bg-white" , "text-dark"]}
            if(card == "estudiante"){login.class_card_estudiante = ["bg-white" , "text-dark"]}
        },

        //=======================
        // REGISTRO :
        //======================
        SelectViewRegister:function(view){
            // si cambias el  rol o la vista de registro .. las
            // variables de registro deben ser formateadas:
                this.nombres_estudiante =  "";
                this.apellidos_estudiante =  "";
                this.nombres_docente =  "" ;
                this.apellidos_docente = "";
                this.nombre_colegio = "";
                this.correo = "";
                this.contrasena = "";
                this.rol= "";
            if(view == "colegio"){
                this.registro[0]["select_rol"]= false;
                this.registro[0]["form_colegio"]= true;
                this.registro[0]["form_docente"]= false;
                this.registro[0]["form_estudiante"]= false;
                this.rol = "colegio";
            }else if(view == "docente"){
                this.registro[0]["select_rol"]= false;
                this.registro[0]["form_colegio"]= false;
                this.registro[0]["form_docente"]= true;
                this.registro[0]["form_estudiante"]= false;
                this.rol = "docente";
            }else if(view == "estudiante"){
                this.registro[0]["select_rol"]= false;
                this.registro[0]["form_colegio"]= false;
                this.registro[0]["form_docente"]= false;
                this.registro[0]["form_estudiante"]= true;
                this.rol = "estudiante";
            }else if(view == "selectRol"){
                this.registro[0]["select_rol"]= false;
                this.registro[0]["form_colegio"]= false;
                this.registro[0]["form_docente"]= false;
                this.registro[0]["form_estudiante"]= false;
                this.iniciar_sesion = false;
                this.registro[0]["select_rol"]= true;
            }else if(view == "iniciar_sesion"){
                this.registro[0]["select_rol"]= false;
                this.registro[0]["form_colegio"]= false;
                this.registro[0]["form_docente"]= false;
                this.registro[0]["form_estudiante"]= false;
                this.iniciar_sesion = true;
                this.registro[0]["select_rol"]= false;
            }
        },

        RegistrarUsuario : function (){
            this.procesando = true;
            let formdata = new FormData();
            formdata.append("RegistrarUsuario" , true);
            formdata.append("rol",this.rol);
            formdata.append("nombres_estudiante",this.nombres_estudiante);
            formdata.append("apellidos_estudiante",this.apellidos_estudiante);
            formdata.append("nombres_docente",this.nombres_docente);
            formdata.append("apellidos_docente",this.apellidos_docente);
            formdata.append("nombre_colegio",this.nombre_colegio);
            formdata.append("correo",this.correo);
            formdata.append("contrasena",this.contrasena);

            axios.post(SERVERURL+"Controllers/CtrlLogin.php", formdata)
            .then(function (response){
                console.log(response);
                let php  = response.data;
                if(php.respuesta == "acceso_ok"){
                    login.procesando = false;
                    window.location=SERVERURL+"perfil/";
                }else if(php.respuesta == "nombres_docente_null" || 
                         php.respuesta == "apellidos_docente_null" ||
                         php.respuesta == "apellidos_estudiante_null" ||
                         php.respuesta == "nombres_estudiante_null" || 
                         php.respuesta == "nombre_colegio_null" ){
                    login.alerts("Todos los campos son obligatorios","danger");
                    login.procesando = false;
                }else if(php.respuesta == "apellidos_estudiante_pregmatch" ||
                        php.respuesta == "nombres_estudiante_pregmatch" ||
                        php.respuesta == "nombre_colegio_pregmatch" ||
                        php.respuesta == "apellidos_docente_pregmatch" ||
                        php.respuesta == "nombres_docente_pregmatch"){
                    login.alerts("Los campos (apellidos y nombres) no pueden tener carácteres especiales, solo letras.","danger");
                    login.procesando = false;
                }else if(php.respuesta == "formato_correo_novalido"){
                    login.alerts("El correo ingresado no es válido","danger");
                    login.procesando = false;
                }else if(php.respuesta == "email_existente"){
                    login.alerts("¿Ya tienes una cuenta con este correo? Porfavor inicia sesión.","danger");
                    login.procesando = false;
                }
            })
        },


         //=======================
        // INICIAR SESIÓN :
        //======================
        iniciarSesion : function (){
            login.procesando = true;
            let formdata = new FormData();
            formdata.append("iniciar_session" , true);
            formdata.append("correo" , this.correo_);
            formdata.append("contrasena" , this.contrasena_);
            axios.post(SERVERURL+"Controllers/CtrlLogin.php",formdata)
            .then(function (response){
                console.log(response);
                let php = response.data;
                if(php.respuesta == "acceso_ok"){
                    login.procesando = false;
                    location.reload();
                }else if(php.respuesta =="formato_correo_novalido"
                        || php.respuesta == "user_not_found"){
                    login.procesando = false;
                    login.alerts("No hemos reconocido el correo electrónico que ingresaste","danger");
                }else if(php.respuesta =="incorrect_pass"){
                    login.procesando = false;
                    login.alerts("La contraseña es incorrecta","danger");
                }
            })
        },


  
     
        alerts: function (string,type){
            if(type == "danger"){
                var title = "Error!";
                var icon = "fa fa-exclamation-circle";
                var alert_color = "#37AAB9";
            }else if(type == "success"){
                var title = "Ok!";
                var icon = "fa fa-check";
                var alert_color = "#37AAB9";
            }
             ///alert de aviso error:
             $.toast().reset('all');
             $("body").removeAttr('class').addClass("top-center-fullwidth");
             $.toast({
                heading :'<i class="jq-toast-icon '+icon+'"></i>'+ string,
                //text: '<i class="jq-toast-icon '+icon+'"></i><p>'+string+'</p>',
                position: 'top-center',
                loaderBg:'#185F8D',
                bgColor: alert_color,
                class: 'jq-has-icon jq-toast-dark',
                hideAfter: 5500, 
                stack: 6,
                showHideTransition: 'slide'
             });
             ///Fin alert de aviso error
        }
        




    },
})


