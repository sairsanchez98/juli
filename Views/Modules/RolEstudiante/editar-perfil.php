
    <!-- Main Content -->
    <div class="hk-pg-wrapper" id="editarPerfil">
        <!-- Container -->
        <div class="container-fluid">
            <!-- Row -->
            <div class="row">
                <div class="col-xl-12 pa-0">
                   
                <div class="tab-content mt-sm-60 mt-30">
                        <div class="tab-pane fade show active" role="tabpanel">
                            <div class="container">
                                <div class="hk-row">
                                    <div class="col-lg-12">
                                        <div class="card card-profile-feed">
                                            <div class="card-header card-header-action">
                                                <div class="media align-items-center">
                                                    <div class="media-img-wrap d-flex mr-10">
                                                        <div class="avatar avatar-sm">
                                                            <img src="<?php echo SERVERURL; ?>Assets/dist/img/iconos/editar.png" alt="user" class="avatar-img img">
                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="font-weight-500 text-dark">Edita tu información</div>
                                                    </div>
                                                </div>
                                                <div class="d-flex align-items-center card-action-wrap">
                                                    <div class="inline-block dropdown">
                                                        <a class="dropdown-toggle no-caret" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="ion ion-ios-more"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="#">Action</a>
                                                            <a class="dropdown-item" href="#">Another action</a>
                                                            <a class="dropdown-item" href="#">Something else here</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a class="dropdown-item" href="#">Separated link</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                
                                                <!-- FORMULARIO DE EDICIÓN DE INFORMACIÓN -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-12 form-group">
                                                                <label for="nombres">Nombres</label>
                                                                <input v-model="nombres" id="nombres" type="text" class="form-control rounded-input" placeholder="Nombres">
                                                            </div>
                                                            <div class="col-md-12 form-group">
                                                                <label for="apellidos">Apellidos</label>
                                                                <input v-model="apellidos" id="apellidos" type="text" class="form-control rounded-input" placeholder="Apellidos">
                                                            </div>
                                                            <div class="col-md-12 form-group">
                                                                <label for="correo">Correo</label>
                                                                <input v-model="correo" id="correo" type="email" class="form-control rounded-input" placeholder="Correo electrónico">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                        <label >Cambia tu foto de perfil</label>
                                                        <input type="file" id="foto" class="dropify" 
                                                        data-default-file="<?php echo SERVERURL. $USER_LOG['foto']?>"
                                                        data-max-file-size="3M" data-allowed-file-extensions="png jpg jpeg bmp gif"/>
                                                    </div>

                                                    <div class="col-md-12 col-xl-12 col-sm-12 form-group mt-10">
                                                        <button v-if="!procesando" @click="EditarPerfil()" class="btn btn-primary btn-rounded btn-block"><i class="fa fa-save"></i> Guardar cambios</button>
                                                        <button v-if="procesando" class="btn btn-primary btn-rounded btn-block">
                                                            <img class="img-fluid" width="16" src="<?php echo SERVERURL; ?>Assets/dist/img/loader.gif"> Cargando
                                                        </button>
                                                        
                                                    </div>
                                                   
                                                </div>


                                            </div>
                                            <!-- <div class="card-footer justify-content-between">
                                                <div>
                                                    <a href="#"><i class="ion ion-md-heart text-primary"></i>30K</a>
                                                </div>
                                                <div>
                                                    <a href="#">1K comments</a>
                                                    <a href="#">12 shares</a>
                                                </div>
                                            </div> -->
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                   	
                </div>
            </div>
            <!-- /Row -->
        </div>
        <!-- /Container -->

      

    </div>
    <!-- /Main Content -->




    
    <!-- Dropify JavaScript -->
    <script src="<?php echo SERVERURL ; ?>Assets/vendors/dropify/dist/js/dropify.min.js"></script>
	
    <!-- JS VUE -->
    <script src="<?php echo SERVERURL ; ?>Views/JS/EstudianteEditarPerfil.js"></script>

    
