<?php
require_once"conexion.php";

class Usuarios {


    static public function CargarInformacion($tabla, $item, $value, $orden, $itemOrden){
        if ($item !== null) {
            $conn = Conection::conectar()->prepare("SELECT * FROM  $tabla WHERE $item = $value  ");
            $conn -> execute();
            return $conn->fetchAll();
        }else{
            if ($orden == "ASC") {
                $conn = Conection::conectar()->prepare("SELECT * FROM $tabla ORDER BY $itemOrden ASC ");
                $conn -> execute();
                return $conn->fetchAll();
            }else{
                $conn = Conection::conectar()->prepare("SELECT * FROM $tabla ORDER BY $itemOrden DESC ");
                $conn -> execute();
                return $conn->fetchAll();
            }
        }
    }


    static public function EditarInformacion($id_usuario, $id_rol, $rol, $foto, $nombre, $apellidos, $correo){

        ### PRIMERO ACTUALIZAMOS LOS DATOS QUE TODOS LOS ROLES PUEDEN TENER
        ## USUARIO (CORREO Y FOFO)
        $stmt_correo = Conection::conectar()->prepare("UPDATE usuarios 
        SET email = :correo WHERE id = :id");
        $stmt_correo->bindParam(":correo",$correo, PDO::PARAM_STR);
        $stmt_correo->bindParam(":id", $id_usuario, PDO::PARAM_INT);
        $stmt_correo->execute();

        if ($foto !== "") { // si el usuario cambió la foto
            $stmt_foto = Conection::conectar()->prepare("UPDATE usuarios 
            SET foto = :foto WHERE id = :id");
            $stmt_foto->bindParam(":foto",$foto, PDO::PARAM_STR);
            $stmt_foto->bindParam(":id", $id_usuario, PDO::PARAM_INT);
            $stmt_foto->execute();
        }

        if ($rol == "colegio") {
            $stmt_colegio = Conection::conectar()->prepare("UPDATE rol_colegios 
            SET nombre = :nombre WHERE id = :id");
            $stmt_colegio->bindParam(":nombre",$nombre, PDO::PARAM_STR);
            $stmt_colegio->bindParam(":id", $id_rol, PDO::PARAM_INT);
            $stmt_colegio->execute();
        }else{
            $db_rol_ = "rol_".$rol."s";
            $stmt_does = Conection::conectar()->prepare("UPDATE $db_rol_
            SET nombre = :nombre , apellidos = :apellidos  WHERE id = :id");
            $stmt_does->bindParam(":nombre",$nombre, PDO::PARAM_STR);
            $stmt_does->bindParam(":apellidos",$apellidos, PDO::PARAM_STR);
            $stmt_does->bindParam(":id", $id_rol, PDO::PARAM_INT);
            $stmt_does->execute();
        }

    }

}