<?php
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

class CtrlTemplate{

	static public function CtrlPlantillaEstudiantes()
	{
		include "Views/template/template_estudiantes.php";
	}	

	static public function CtrlPlantillaLogin(){
		include "Views/template/templateLogin.php";
	}


	static public function CtrlPlantillaDocentesYColegios()
	{
		include "Views/template/template_docentes_y_colegios.php";
	}


	static public function CargarRuta(){
		$USER_LOG = $_SESSION["UserLoggedIn"]["user"];
		$ROL = $USER_LOG["rol"];
		
		if (isset($_GET["_RuTa_"])) {
			// MÓDULOS ESTUDIANTES: 
			########################
			if ($ROL == "estudiante")
			{
				##ruta get:
				$ruta = explode("/",$_GET["_RuTa_"]);
				## rutas/vistas permitidas:				
				$views = array(
					["perfil","editar-perfil"],
					["seguimiento"],["premios"],["comunidad"],["aprender-mas"]
				); 	
				
				## procesar:
				if (count($ruta) == 1 || (count($ruta) == 2 && $ruta[1]=="" )) {
					for ($view=0; $view < count($views) ; $view++) { 
						if ($ruta[0] == $views[$view][0]) {
							return "Views/Modules/RolEstudiante/".$ruta[0].".php";
						break;
						}else{
							return "Views/Modules/404.php";
						}
					}
				}else if (count($ruta) == 2 || (count($ruta) == 3 && $ruta[2]=="" )) {
					for ($view=0; $view < count($views) ; $view++) { 
						if ($ruta[0] == $views[$view][0]) {
							if ($ruta[1] == $views[$view][1]) {
								return "Views/Modules/RolEstudiante/".$ruta[1].".php";
							break;
							}
						}else{
							return "Views/Modules/404.php";
						}
					}
				}else if (count($ruta) >= 3 && $ruta[2] !== "") {
					return "Views/Modules/404.php";
				}
				
			}
			
			// MÓDULOS COLEGIO: 
			########################
			if ($ROL == "colegio")
			{
				# code...
			}
			
			// MÓDULOS DOCENTE: 
			########################
			if ($ROL == "docente")
			{
				# code...
			}

			
		}else{
		 	return "Views/Modules/RolEstudiante/Inicio.php";
		}

	}

}

/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/