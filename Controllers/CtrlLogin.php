<?php
    require_once"../Models/MdlLogin.php";
    require_once"../Models/MdlUsuarios.php";
    

     require_once "../Ext/carbon/vendor/autoload.php";
     use Carbon\Carbon;
     date_default_timezone_set('America/Bogota');
     Carbon::setLocale('es');
     $fechaActual = Carbon::now()->toDateTimeString();


    if (isset($_POST["iniciar_session"])) {
        sleep (1);
        
        $USUARIO = $_POST["correo"];
        $CONTRASENA = crypt($_POST["contrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        
        
        ## analizamos que los CARACTERES ingresados por el visitante en el campo de USUARIO
        ## correspondan bien y no sean caracteres especiales ...
        if (filter_var($USUARIO, FILTER_VALIDATE_EMAIL)) 
        {
             ## Validamos el usuario (verificamos si es un email): 
            $ValidarUsuario = MdlLogin::ValidacionDeUsuario($USUARIO , "email");
            if ($ValidarUsuario) { ## si el usuario fue encontrado como EMAIL entonces...
                ##procedemos a validar la contraseña
                if ($ValidarUsuario[0]["contrasena"] == $CONTRASENA) {
                    ## Definir ruta de la foto del usuario:
                    if ($ValidarUsuario[0]["foto"] == "") {
                        $foto = "Assets/dist/img/iconos/default_avatar_".$ValidarUsuario[0]["rol"].".png";
                    }else{
                        $foto = "Views/upload/usuarios/".$ValidarUsuario[0]["foto"];
                    }

                    if ($ValidarUsuario[0]["rol"] == "colegio") {
                        $CARGAR_INFO = Usuarios::CargarInformacion(
                            "rol_colegios" , "id_usuario" , $ValidarUsuario[0]["id"] , "DESC" , "id"
                        );

                        
                        $info_user = array(
                            "nombre" => $CARGAR_INFO[0]["nombre"],
                            "id_rol" => $CARGAR_INFO[0]["id"],
                            "id_usuario" => $ValidarUsuario[0]["id"],
                            "token" => $CARGAR_INFO[0]["token"],
                            "email" => $ValidarUsuario[0]["email"],
                            "rol" => $ValidarUsuario[0]["rol"],
                            "sub_rol" => "",
                            "foto"=> $foto
                        );
                    }else if ($ValidarUsuario[0]["rol"] == "docente"){
                        $CARGAR_INFO = Usuarios::CargarInformacion(
                            "rol_docentes" , "id_usuario" , $ValidarUsuario[0]["id"] , "DESC" , "id"
                        );

                        $info_user = array(
                            "nombre" => $CARGAR_INFO[0]["nombre"],
                            "apellidos" => $CARGAR_INFO[0]["apellidos"],
                            "id_rol" => $CARGAR_INFO[0]["id"],
                            "id_usuario" => $ValidarUsuario[0]["id"],
                            "email" => $ValidarUsuario[0]["email"],
                            "rol" => $ValidarUsuario[0]["rol"],
                            "sub_rol" => $ValidarUsuario[0]["sub_rol"],
                            "foto"=> $foto
                        );
                    }else if ($ValidarUsuario[0]["rol"] == "estudiante"){
                        $CARGAR_INFO = Usuarios::CargarInformacion(
                            "rol_estudiantes" , "id_usuario" , $ValidarUsuario[0]["id"] , "DESC" , "id"
                        );

                        $info_user = array(
                            "nombre" => $CARGAR_INFO[0]["nombre"],
                            "apellidos" => $CARGAR_INFO[0]["apellidos"],
                            "id_rol" => $CARGAR_INFO[0]["id"],
                            "id_usuario" => $ValidarUsuario[0]["id"],
                            "email" => $ValidarUsuario[0]["email"],
                            "rol" => $ValidarUsuario[0]["rol"],
                            "sub_rol" => $ValidarUsuario[0]["sub_rol"],
                            "foto"=> $foto
                        );
                    }


                    $REST["respuesta"] = "acceso_ok";## si el usuario y contraseña ya fueron validados
                    ## y el acceso es OK entonces creamos la sesion:
                    session_start();
                    $USER_LOGUED = array("user" => $info_user ,  "estado"=>"ok"); 
                    ## aquí he puesto en un array el id del usuario logueado para tener la info de él
                    ## y también defino que el estado de la sesion es OK!
                    $_SESSION["UserLoggedIn"] = $USER_LOGUED; ## INGRESO LOS DATOS A LA SESION
                    $REST["_session_"] = $_SESSION["UserLoggedIn"];
                }else{
                    $REST["respuesta"] = "incorrect_pass";
                }
            }else{ 
                ## si  fue encontrado como usuario 
                    ## mandamos una respuesta de "USUARIO NO ENCONTRADO"
                    $REST["respuesta"] = "user_not_found";
            }
        }
        else
        {
            $REST["respuesta"] = "formato_correo_novalido";
        }



        header("Content-Type: application/json");
        echo json_encode($REST);   
    }




    if (isset($_POST["RegistrarUsuario"])) {
        sleep(1);
        ## datos generales:
        $CONTRASENA = crypt($_POST["contrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        $CORREO = $_POST["correo"];
        $ROL = $_POST["rol"];
        ####################################
        ## REGISTRANDO A UN ESTUDIANTE: 
        ###################################
        if ($_POST["rol"] == "estudiante") {
            $nombres_estudiante_null = str_replace(" ","",$_POST["nombres_estudiante"]);
            $apellidos_estudiante_null = str_replace(" ","",$_POST["apellidos_estudiante"]);
            
                if($nombres_estudiante_null  !== ""){
                    if($apellidos_estudiante_null  !== ""){
                        if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["nombres_estudiante"])) {
                            if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["apellidos_estudiante"])) {
                                   
                                if (filter_var($CORREO, FILTER_VALIDATE_EMAIL)) {
                                    
                                    // validar correo existente:
                                        ## VALIDAMOS QUE EL CORREO NO ESTÉ REGISTRADO EN LA DB 
                                        $EncotrarUsuario = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                                            if (!$EncotrarUsuario) { // si el correo no existe 
                                                    ## procedemos a registrar el usuario :
                                                    $REGISTRAR_USUARIO = MdlLogin::RegistrarUsuario(
                                                    $ROL , $CORREO , $CONTRASENA
                                                    );

                                                // Si se registra el usuario entonces: 
                                                    # registramos la información del rol:
                                                    ## para eso buscamos al usuario para registrarle el ID: 
                                                if ($REGISTRAR_USUARIO) { 
                                                        $UsuarioRegistrado = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                                                        

                                                    
                                                        $REGISTRAR_ESTUDIANTE = MdlLogin::RegistrarEstudiante(
                                                            $UsuarioRegistrado[0]["id"] ,  $_POST["nombres_estudiante"] ,
                                                            $_POST["apellidos_estudiante"] , $fechaActual , $fechaActual
                                                        );
                            
                                                        if ($REGISTRAR_ESTUDIANTE) {
                                                            $CARGAR_INFO = Usuarios::CargarInformacion(
                                                                "rol_estudiantes" , "id_usuario" , $UsuarioRegistrado[0]["id"] , "DESC" , "id"
                                                            );

                                                            
                                                            $foto = "Assets/dist/img/iconos/default_avatar_".$ROL.".png";
                                                            $info_user = array(
                                                                "nombre" => $CARGAR_INFO[0]["nombre"],
                                                                "apellidos" => $CARGAR_INFO[0]["apellidos"],
                                                                "id_rol" => $CARGAR_INFO[0]["id"],
                                                                "id_usuario" => $UsuarioRegistrado[0]["id"],
                                                                "email" => $UsuarioRegistrado[0]["email"],
                                                                "rol" => $UsuarioRegistrado[0]["rol"],
                                                                "sub_rol" => $UsuarioRegistrado[0]["sub_rol"],
                                                                "foto"=> $foto
                                                            );
                            
                            
                            
                                                            $REST["respuesta"] = "acceso_ok"; 
                                                            session_start();
                                                            $USER_LOGUED = array("user" => $info_user ,  "estado"=>"ok"); 
                                                            ## aquí he puesto en un array el id del usuario logueado para tener la info de él
                                                            ## y también defino que el estado de la sesion es OK!
                                                            $_SESSION["UserLoggedIn"] = $USER_LOGUED; ## INGRESO LOS DATOS A LA SESION  
                                                        }else{
                                                            $REST["respuesta"] = "err500";
                                                        }                                                    

                                                }
                                            }else{
                                                $REST["respuesta"] = "email_existente";
                                            }

                                }else{
                                    $REST["respuesta"] = "formato_correo_novalido";    
                                }
                                   
                            }else{
                                $REST["respuesta"] = "apellidos_estudiante_pregmatch";
                            }
                        }else{
                            $REST["respuesta"] = "nombres_estudiante_pregmatch";
                        }
                    }else{
                        $REST["respuesta"] = "apellidos_estudiante_null";
                    }
                }else{
                    $REST["respuesta"] = "nombres_estudiante_null";
                }
               
        }

        ###################################
        ## REGISTRANDO A UN DOCENTE:
        ##################################
        if ($_POST["rol"] == "docente" ) {
            $nombres_docente_null = str_replace(" ","",$_POST["nombres_docente"]);
            $apellidos_docente_null = str_replace(" ","",$_POST["apellidos_docente"]);

            if($nombres_docente_null  !== ""){
                if($apellidos_docente_null  !== ""){
                       
                    if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["nombres_docente"])) {
                        if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["apellidos_docente"])) {
                            if (filter_var($CORREO, FILTER_VALIDATE_EMAIL)) {
                                
                                // validar correo existente:
                                    ## VALIDAMOS QUE EL CORREO NO ESTÉ REGISTRADO EN LA DB 
                                    $EncotrarUsuario = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                                        if (!$EncotrarUsuario) { // si el correo no existe 
                                                ## procedemos a registrar el usuario :
                                                $REGISTRAR_USUARIO = MdlLogin::RegistrarUsuario(
                                                $ROL , $CORREO , $CONTRASENA
                                                );

                                            // Si se registra el usuario entonces: 
                                                # registramos la información del rol:
                                                ## para eso buscamos al usuario para registrarle el ID: 
                                            if ($REGISTRAR_USUARIO) { 
                                                    $UsuarioRegistrado = MdlLogin::ValidacionDeUsuario($CORREO , "email");

                                                
                                                    $REGISTRAR_DOCENTE = MdlLogin::RegistrarDocente(
                                                        $UsuarioRegistrado[0]["id"] ,  $_POST["nombres_docente"] ,
                                                        $_POST["apellidos_docente"],$fechaActual , $fechaActual
                                                    );
                        
                                                    if ($REGISTRAR_DOCENTE) {
                        
                                                        $CARGAR_INFO = Usuarios::CargarInformacion(
                                                            "rol_docentes" , "id_usuario" , $UsuarioRegistrado[0]["id"] , "DESC" , "id"
                                                        );
                                                        $foto = "Assets/dist/img/iconos/default_avatar_".$ROL.".png";
                                                        $info_user = array(
                                                            "nombre" => $CARGAR_INFO[0]["nombre"],
                                                            "apellidos"=> $CARGAR_INFO[0]["apellidos"],
                                                            "id_rol" => $CARGAR_INFO[0]["id"],
                                                            "id_usuario" => $UsuarioRegistrado[0]["id"],
                                                            "email" => $UsuarioRegistrado[0]["email"],
                                                            "rol" => $UsuarioRegistrado[0]["rol"],
                                                            "sub_rol" => $UsuarioRegistrado[0]["sub_rol"],
                                                            "foto"=>$foto
                                                        );
                        
                                                        $REST["respuesta"] = "acceso_ok";
                                                        session_start();
                                                        $USER_LOGUED = array("user" => $info_user ,  "estado"=>"ok"); 
                                                        ## aquí he puesto en un array el id del usuario logueado para tener la info de él
                                                        ## y también defino que el estado de la sesion es OK!
                                                        $_SESSION["UserLoggedIn"] = $USER_LOGUED; ## INGRESO LOS DATOS A LA SESION
                                                    }else{
                                                        $REST["respuesta"] = "err500";
                                                    }
                                             
                                            }
                                        }else{
                                            $REST["respuesta"] = "email_existente";
                                        }

                            }else{
                                $REST["respuesta"] = "formato_correo_novalido";    
                            }
                        }else{
                            $REST["respuesta"] = "apellidos_docente_pregmatch";
                        }
                    }else{
                        $REST["respuesta"] = "nombres_docente_pregmatch";
                    }
                           
                }else{
                    $REST["respuesta"] = "apellidos_docente_null";
                }
            }else{
                $REST["respuesta"] = "nombres_docente_null";
            }
        }


        ###################################
        ## REGISTRANDO A UN COLEGIO:
        ##################################
        if ($_POST["rol"] == "colegio") {
             ## datos generales:
            $CONTRASENA = crypt($_POST["contrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
            $CORREO = $_POST["correo"];
            $ROL = $_POST["rol"];

            $nombre_colegio_null = str_replace(" ","",$_POST["nombre_colegio"]);
            if ($nombre_colegio_null !== "") {
                if (preg_match('/^[a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["nombre_colegio"])) {
                    if (filter_var($CORREO, FILTER_VALIDATE_EMAIL)) {
                        // validar correo existente:
                        ## VALIDAMOS QUE EL CORREO NO ESTÉ REGISTRADO EN LA DB 
                            $EncotrarUsuario = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                            if (!$EncotrarUsuario) { // si el correo no existe 
                                ## procedemos a registrar el usuario :
                                $REGISTRAR_USUARIO = MdlLogin::RegistrarUsuario(
                                $ROL , $CORREO , $CONTRASENA
                                );

                                // Si se registra el usuario entonces: 
                                    # registramos la información del rol:
                                    ## para eso buscamos al usuario para registrarle el ID: 
                                if ($REGISTRAR_USUARIO) { 
                                    $UsuarioRegistrado = MdlLogin::ValidacionDeUsuario($CORREO , "email");

                                        $token = substr($_POST["nombre_colegio"],0,3).(($UsuarioRegistrado[0]["id"]+33*7)+7).$UsuarioRegistrado[0]["id"];
                        
                                        $REGISTRAR_COLEGIO = MdlLogin::RegistrarColegio(
                                            $UsuarioRegistrado[0]["id"] , $token , $_POST["nombre_colegio"] ,
                                            $fechaActual , $fechaActual
                                        );
                                        if ($REGISTRAR_COLEGIO) {
                            
                                            $CARGAR_INFO = Usuarios::CargarInformacion(
                                                "rol_colegios" , "id_usuario" , $UsuarioRegistrado[0]["id"] , "DESC" , "id"
                                            );
                                            $foto = "Assets/dist/img/iconos/default_avatar_".$ROL.".png";
                                            $info_user = array(
                                                "nombre" => $CARGAR_INFO[0]["nombre"],
                                                "id_rol" => $CARGAR_INFO[0]["id"],
                                                "id_usuario" => $UsuarioRegistrado[0]["id"],
                                                "token" => $CARGAR_INFO[0]["token"],
                                                "email" => $UsuarioRegistrado[0]["email"],
                                                "rol" => $UsuarioRegistrado[0]["rol"],
                                                "sub_rol" => "",
                                                "foto"=>$foto
                                            );
            
                                            $REST["respuesta"] = "acceso_ok";       
                                            session_start();
                                            $USER_LOGUED = array("user" => $info_user ,  "estado"=>"ok"); 
                                            ## aquí he puesto en un array el id del usuario logueado para tener la info de él
                                            ## y también defino que el estado de la sesion es OK!
                                            $_SESSION["UserLoggedIn"] = $USER_LOGUED; ## INGRESO LOS DATOS A LA SESION
                                        }else{
                                            $REST["respuesta"] = "err500";
                                        }

                                }
                            }else{
                                $REST["respuesta"] = "email_existente";                
                            }

                    }else{
                        $REST["respuesta"] = "formato_correo_novalido";        
                    }
                }else{
                    $REST["respuesta"] = "nombre_colegio_pregmatch";    
                }
            }else{
                $REST["respuesta"] = "nombre_colegio_null";
            }

        }





        

        header("Content-Type: application/json");
        echo json_encode($REST);   
    }








    if (isset($_GET["destroySession"])) {
        session_start();
        $_SESSION["UserLoggedIn"]["estado"] = false;
        session_destroy();

        $REST["session"] = "destroy";

        header("Content-Type: application/json");
        echo json_encode($REST);   
    }













    


?>