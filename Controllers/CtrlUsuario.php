<?php
session_start();
require_once"../Models/MdlUsuarios.php";
require_once"../Models/MdlLogin.php";


##=========================================
// CARGAR INFORMACIÓN ACTUAL
##=========================================
if (isset($_GET["cargarInformacionActual"])) {
    $REST["respuesta"] = $_SESSION["UserLoggedIn"]["user"];
    header("Content-Type: application/json");
    echo json_encode($REST);   
}



##=========================================
//EDITANDO USUARIOS (ESTUDIANTES Y DOCENTES)
##=========================================
if (isset($_POST["EditarPerfil"])) {
    sleep(1);
    $USER_LOG = $_SESSION["UserLoggedIn"]["user"];
    
    ## validar variables
    $CORREO = $_POST["correo"];
    $NOMBRE = $_POST["nombres"];
    $APELLIDOS = $_POST["apellidos"];

    $nombres_null = str_replace(" ","",$NOMBRE);
    $apellidos_null = str_replace(" ","",$APELLIDOS);
    
    if($nombres_null  !== ""){
        if($apellidos_null  !== ""){
            if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ ]+$/' , $NOMBRE)) {
                if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ ]+$/' , $APELLIDOS)) {
                    if (filter_var($CORREO, FILTER_VALIDATE_EMAIL)) {
                        
                        //validamos que el correo nuevo no se encuentre registrado
                        $EncotrarUsuario = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                        // si el correo fue encontrado nos debemos fijar que sea el del usuario logueado
                        // ya que si no es el de el usuario actualmente logueado .. este le pertenece a otro 
                        $correo_existente = 0;
                        foreach ($EncotrarUsuario as $key => $us) {
                            if ($us["email"]!== $USER_LOG["email"]) {
                                $correo_existente  = 1; // 
                            break;
                            }
                        }

                        if ($correo_existente == 0) {

                            // si ya pasamos todos los filtros 
                            // entonces procedemos a actualizar la DB
                             ## si existe cambio de foto 
                            ## procesamos el archivo:
                            $errorFoto = false;
                            if (isset($_FILES["foto"])) {
                                $FOTO = $_FILES["foto"];
                                if ($FOTO["type"] == "image/jpg" || $FOTO["type"] == "image/gif"||
                                $FOTO["type"] == "image/jpeg" || $FOTO["type"] == "image/bmp"||
                                $FOTO["type"] == "image/png") 
                                {
                                    $archivo_temporal =  $FOTO["tmp_name"];
                                    $nombre_archivo = $FOTO["name"];
                                    $directorio = "../Views/upload/usuarios/".$USER_LOG['id_usuario']."/";
                                    if (!file_exists($directorio)) { # cree el directorio si no existe
                                        mkdir($directorio, 0755); // creación con permisos a la carpeta
                                    }else { 
                                        //si el directorio existe debemos borrar todo lo que haya dentro 
                                        $files = glob($directorio."*"); //obtenemos todos los nombres de los ficheros
                                            foreach($files as $file){
                                                if(is_file($file))
                                                unlink($file); //elimino el fichero
                                            }

                                    }
                                    // guardamos el archivo en la ruta: : : 
                                    $ruta = $directorio.$nombre_archivo;
                                    # muevo el archivo 
                                    if (move_uploaded_file($archivo_temporal, $ruta)) {
                                        // registramos la url de la foto en la DB
                                        $Url_foto_db = $USER_LOG['id_usuario']."/".$nombre_archivo;    
                                    }else{
                                        $REST["respuesta"] == "erroAlSubirFoto";
                                        $errorFoto = true;
                                    }

                                    

                                }else{
                                    $REST["respuesta"] == "FormatoDeFotoNoValido";
                                    $errorFoto = true;
                                }
                            }else{
                                $Url_foto_db = "";
                            }

                         if (!$errorFoto) {
                            $editar = Usuarios::EditarInformacion(
                                $USER_LOG["id_usuario"],
                                $USER_LOG["id_rol"],
                                $USER_LOG["rol"],
                                $Url_foto_db ,
                                $NOMBRE,
                                $APELLIDOS,
                                $CORREO
                            );
                            $_SESSION["UserLoggedIn"]["user"]["apellidos"] = $APELLIDOS;
                            $_SESSION["UserLoggedIn"]["user"]["nombre"] = $NOMBRE;
                            $_SESSION["UserLoggedIn"]["user"]["email"] = $CORREO;
                            if ($Url_foto_db !== "") {
                                $_SESSION["UserLoggedIn"]["user"]["foto"] = "Views/upload/usuarios/".$Url_foto_db;
                            }
                            
                            $REST["respuesta"] = "Actualizacion_OK";
                         }

                            
                        }else{
                            $REST["respuesta"]="email_existente";      
                        }

                    }else{$REST["respuesta"] = "formato_correo_novalido";}
                }else{$REST["respuesta"]="preg_match_apellidos";}
            }else{$REST["respuesta"]="preg_match_nombres";}
        }else{$REST["respuesta"]="apellidos_null";}
    }else{$REST["respuesta"]="nombres_null";}

   
     

    
     

    header("Content-Type: application/json");
    echo json_encode($REST);   

}