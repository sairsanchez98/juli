
<?php
session_start();
require_once"rute.php";
require_once"Controllers/CtrlTemplate.php";

    if (isset($_SESSION["UserLoggedIn"]) && @$_SESSION["UserLoggedIn"]["estado"] == "ok") {
        
        $USER_LOG = $_SESSION["UserLoggedIn"]["user"];

        if (@$USER_LOG["rol"] == "estudiante") {
            $plantilla = CtrlTemplate::CtrlPlantillaEstudiantes();
        }else{
            $plantilla = CtrlTemplate::CtrlPlantillaDocentesYColegios();
        }
    }else{
        $plantilla = CtrlTemplate::CtrlPlantillaLogin();
    }
    
?>